const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
    name: { type: String },
    lastName: { type: String }
});

module.exports = mongoose.model('users', user);