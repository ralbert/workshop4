import React, { Component } from 'react';
import User from './components/User';  // importa el componente ya creado WeatherLocation
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <User></User>
      </div>
    );
  }
}

export default App;
